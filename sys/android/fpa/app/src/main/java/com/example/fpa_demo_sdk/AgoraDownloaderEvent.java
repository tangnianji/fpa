package com.example.fpa_demo_sdk;

public enum AgoraDownloaderEvent {
    FPA_DL_EVENT_IDLE(0),
    FPA_DL_EVENT_START(1),
    FPA_DL_EVENT_END(2),
    FPA_DL_EVENT_FAILED(3),
    FPA_DL_EVENT_DISCONNECTED(4),
    FPA_DL_EVENT_CONNECTING(5),
    FPA_DL_EVENT_CONNECTED(6);

    int value;
    private AgoraDownloaderEvent(int v){
        value = v;
    }

    public int getValue() {
        return value;
    }
}

package com.example.fpa_demo_sdk;

public class AgoraDownloadResult {
    private double totalTime;
    private double avgDlSpeed;
    private double connTimeCast;

    public double getTotalTime() {
        return totalTime;
    }

    public double getConnTimeCast() {
        return connTimeCast;
    }

    public double getAvgDlSpeed() {
        return avgDlSpeed;
    }
}

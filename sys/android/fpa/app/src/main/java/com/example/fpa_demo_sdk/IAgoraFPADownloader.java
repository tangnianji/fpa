package com.example.fpa_demo_sdk;

public abstract class IAgoraFPADownloader {
    public abstract int startDownload(AgoraFPADownloadConfig downloadConfig);

    public abstract int stopDownload();

    public abstract void registerDownloadObserver(IAgoraFPADownloaderObserver observer);

    public abstract void unregisterDownloadObserver();
}

package com.example.fpa_demo_sdk;

public class AgoraFPADownloader extends IAgoraFPADownloader{
    private long nativeDownloader = 0;
    private static boolean nativeLibraryLoad = false;

    public AgoraFPADownloader(){
        libraryLoad();
        nativeDownloader = nativeCreateDownloader();
    }

    private static int libraryLoad(){
        if(nativeLibraryLoad){
            return 0;
        }
        try{
            System.loadLibrary("agora_fpa_sdk_jni");
            nativeLibraryLoad = true;
            return 0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if(nativeDownloader != 0){
            nativeDeleteDownloader(nativeDownloader);
        }
        nativeDownloader = 0;
    }

    @Override
    public int startDownload(AgoraFPADownloadConfig downloadConfig){
        return nativeStartDownload(nativeDownloader, downloadConfig);
    }

    @Override
    public int stopDownload(){
        return nativeStopDownload(nativeDownloader);
    }

    @Override
    public void registerDownloadObserver(IAgoraFPADownloaderObserver observer){
        nativeRegisterDownloadObserver(nativeDownloader, observer);
    }

    @Override
    public void unregisterDownloadObserver(){
        nativeUnregisterDownloadObserver(nativeDownloader);
    }

    private native int nativeStartDownload(long nativeDownloader, AgoraFPADownloadConfig downloadConfig);

    private native int nativeStopDownload(long nativeDownloader);

    private native void nativeRegisterDownloadObserver(long nativeDownloader, IAgoraFPADownloaderObserver observer);

    private native void nativeUnregisterDownloadObserver(long nativeDownloader);

    private native long nativeCreateDownloader();

    private native void nativeDeleteDownloader(long nativeDownloader);
}

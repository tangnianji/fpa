package com.example.fpa_demo_sdk;

public enum AgoraFPADownloadMode {
    FPA_DL_NORMAL_NETWORK(0),

    FPA_DL_AGORA_FPA(1);

    int value;

    private AgoraFPADownloadMode(int v){
        value = v;
    }

    public int getValue() {
        return value;
    }
}

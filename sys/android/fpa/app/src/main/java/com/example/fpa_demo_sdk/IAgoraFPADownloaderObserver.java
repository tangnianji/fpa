package com.example.fpa_demo_sdk;

public interface IAgoraFPADownloaderObserver {
    void onDownloaderEventCallback(long id, AgoraDownloaderEvent event);

    void onDownloaderSpeedCallback(long id, double kpsPerSec, double dataLoaded);

    void onDownloaderResultCallback(long id, AgoraFPADownloadConfig downloadResult);
}

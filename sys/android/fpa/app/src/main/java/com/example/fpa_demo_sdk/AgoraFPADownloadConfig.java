package com.example.fpa_demo_sdk;

public class AgoraFPADownloadConfig {
    public AgoraFPADownloadMode mode;  // 0: normal download 1: via fpa.
    public String url;         // url for normal download
    public String token;       // token
    public int chanId;       // fpa : channel ID
    public String appId;      // fpa: app id
    public String name;        // fpa : resource name

    public AgoraFPADownloadMode getMode() {
        return mode;
    }

    public int getChanId() {
        return chanId;
    }

    public String getAppId() {
        return appId;
    }

    public String getName() {
        return name;
    }

    public String getToken() {
        return token;
    }

    public String getUrl() {
        return url;
    }
}

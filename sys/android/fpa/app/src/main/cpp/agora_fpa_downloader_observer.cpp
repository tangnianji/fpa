//
// Created by TNJ on 2021/8/8.
//

#include "agora_fpa_downloader_observer.h"

namespace agora{
namespace fpa{

AgoraDownloaderObserver::AgoraDownloaderObserver(JavaVM* jvm, const jobject& java_downloader_observer){
    JNIEnv* env = nullptr;
    jvm->AttachCurrentThread(&env, nullptr);
    java_downloader_observer_ = env->NewGlobalRef(java_downloader_observer);
}

AgoraDownloaderObserver::~AgoraDownloaderObserver(){
    JNIEnv* env = nullptr;
    jvm_->AttachCurrentThread(&env, nullptr);
    env->DeleteGlobalRef(java_downloader_observer_);
}

void AgoraDownloaderObserver::OnDownloaderEventCallback(int64_t id, FPA_DL_EVENT event){

}

void AgoraDownloaderObserver::OnDownloaderSpeedCallback(int64_t id, double kps_per_sec, double data_loaded){

}

void AgoraDownloaderObserver::OnDownloaderResultCallback(int64_t id, const FpaDownloadResult& download_result){

}

}
}

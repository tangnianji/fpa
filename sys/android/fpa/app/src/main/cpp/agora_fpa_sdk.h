//
// Created by TNJ on 2021/8/8.
//

#ifndef FPA_DEMO_SDK_AGORA_FPA_SDK_H
#define FPA_DEMO_SDK_AGORA_FPA_SDK_H

#include <memory>
#include <jni.h>

#include "agora_fpa_downloader.h"
#include "agora_downloader_base.h"

namespace agora{
namespace fpa{

struct AgoraFPASdk{
    AgoraFPASdk(JavaVM* jvm);

    std::shared_ptr<AgoraFpaDownloaderImpl> fpa_downloader_;
    JavaVM* jvm_;
};

}
}

#endif //FPA_DEMO_SDK_AGORA_FPA_SDK_H

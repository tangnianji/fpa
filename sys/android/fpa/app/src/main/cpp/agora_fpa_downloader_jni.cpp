#include <jni.h>
#include <string>
#include "agora_fpa_downloader.h"
#include "agora_downloader_base.h"
#include "agora_fpa_downloader_observer.h"
#include "agora_fpa_sdk.h"

extern "C" JNIEXPORT jint JNICALL
Java_com_example_fpa_1demo_1sdk_AgoraFPADownloader_StartDownload(
        JNIEnv* env, jobject thiz, jlong native_handle, jobject j_download_config){

    agora::fpa::FpaDownloadConfig download_config;
    agora::fpa::AgoraFPASdk* fpa_sdk = reinterpret_cast<agora::fpa::AgoraFPASdk*>(native_handle);
    auto fpa_downloader = fpa_sdk->fpa_downloader_;

    jclass config_class = env->GetObjectClass(j_download_config);
    auto mode_filed_id = env->GetFieldID(config_class, "mode", "Lcom/example/fpa_demo_sdk/AgoraFPADownloadMode;");
    jobject j_mode = env->GetObjectField(j_download_config, mode_filed_id);
    jclass download_config_class = env->GetObjectClass(j_mode);
    jmethodID get_value_method_id = env->GetMethodID(download_config_class, "getValue", "()I");
    auto mode = env->CallIntMethod(j_mode, get_value_method_id);

    auto get_url_method_id = env->GetMethodID(config_class, "getUrl", "()Ljava/lang/String;");
    jstring j_url = static_cast<jstring>(env->CallObjectMethod(j_download_config, get_url_method_id));
    auto url = env->GetStringUTFChars(j_url, JNI_FALSE);

    auto get_chan_id_method_id = env->GetMethodID(config_class, "getChanId", "()I");
    int chan_id = env->CallIntMethod(j_download_config, get_chan_id_method_id);

    auto get_app_id_method_id = env->GetMethodID(config_class, "getAppId", "()Ljava/lang/String;");
    jstring j_app_id = static_cast<jstring>(env->CallObjectMethod(j_download_config, get_app_id_method_id));
    auto app_id = env->GetStringUTFChars(j_app_id, JNI_FALSE);

    auto get_name_method_id = env->GetMethodID(config_class, "getName", "()Ljava/lang/String;");
    jstring j_name = static_cast<jstring>(env->CallObjectMethod(j_download_config, get_name_method_id));
    auto name = env->GetStringUTFChars(j_name, JNI_FALSE);

    auto get_token_method_id = env->GetMethodID(config_class, "getToken", "()Ljava/lang/String;");
    jstring j_token = static_cast<jstring>(env->CallObjectMethod(j_download_config, get_token_method_id));
    auto token = env->GetStringUTFChars(j_token, JNI_FALSE);

    download_config.url = const_cast<char*>(url);
    download_config.app_id = const_cast<char*>(app_id);
    download_config.name = const_cast<char*>(name);
    download_config.token = const_cast<char*>(token);
    download_config.chan_id = chan_id;
    download_config.mode = static_cast<agora::fpa::FPA_DL_MODE>(mode);

    int err = fpa_downloader->StartDownload(download_config);

    env->ReleaseStringUTFChars(j_url, url);
    env->ReleaseStringUTFChars(j_app_id, app_id);
    env->ReleaseStringUTFChars(j_name, name);
    env->ReleaseStringUTFChars(j_token, token);

    return err;
}

extern "C" JNIEXPORT jint JNICALL
Java_com_example_fpa_1demo_1sdk_AgoraFPADownloader_StoptDownload(
        JNIEnv* env, jobject thiz, jlong native_handle){
    agora::fpa::AgoraFPASdk* fpa_sdk = reinterpret_cast<agora::fpa::AgoraFPASdk*>(native_handle);
    auto fpa_downloader = fpa_sdk->fpa_downloader_;
    return fpa_downloader->StopDownload();
}

extern "C" JNIEXPORT void JNICALL
Java_com_example_fpa_1demo_1sdk_AgoraFPADownloader_RegisterDownloadObserver(
        JNIEnv* env, jobject thiz, jlong native_handle, jobject j_observer){
    agora::fpa::AgoraFPASdk* fpa_sdk = reinterpret_cast<agora::fpa::AgoraFPASdk*>(native_handle);
    auto fpa_downloader = fpa_sdk->fpa_downloader_;
    std::shared_ptr<agora::fpa::AgoraDownloaderObserver> observer = std::make_shared<agora::fpa::AgoraDownloaderObserver>(fpa_sdk->jvm_, j_observer);
    fpa_downloader->RegisterDownloadObserver(observer);
}

extern "C" JNIEXPORT void JNICALL
Java_com_example_fpa_1demo_1sdk_AgoraFPADownloader_UnregisterDownloadObserver(
        JNIEnv* env, jobject thiz, jlong native_handle) {
    agora::fpa::AgoraFPASdk* fpa_sdk = reinterpret_cast<agora::fpa::AgoraFPASdk*>(native_handle);
    auto fpa_downloader = fpa_sdk->fpa_downloader_;
    fpa_downloader->UnregisterDownloadObserver();
}

extern "C" JNIEXPORT jlong JNICALL
Java_com_example_fpa_1demo_1sdk_AgoraFPADownloader_CreateDownloader(JNIEnv* env, jobject thiz){
    JavaVM* jvm = nullptr;
    env->GetJavaVM(&jvm);
    auto p = new agora::fpa::AgoraFPASdk(jvm);
    return reinterpret_cast<jlong>(p);
}

extern "C" JNIEXPORT void JNICALL
Java_com_example_fpa_1demo_1sdk_AgoraFPADownloader_DeleteDownloader(JNIEnv* env,
        jobject thiz, jlong native_handle) {
    agora::fpa::AgoraFPASdk* fpa_sdk = reinterpret_cast<agora::fpa::AgoraFPASdk*>(native_handle);
    delete fpa_sdk;
}


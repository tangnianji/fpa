//
// Created by TNJ on 2021/8/8.
//

#ifndef FPA_DEMO_SDK_AGORA_FPA_DOWNLOADER_OBSERVER_H
#define FPA_DEMO_SDK_AGORA_FPA_DOWNLOADER_OBSERVER_H

#include <jni.h>
#include "agora_downloader_base.h"
#include "agora_downloader_callback.h"

namespace agora{
namespace fpa{

class AgoraDownloaderObserver : public IAgoraDownloaderObserver{
public:
    explicit AgoraDownloaderObserver(JavaVM* jvm, const jobject& java_downloader_observer);

    ~AgoraDownloaderObserver();

    void OnDownloaderEventCallback(int64_t id, FPA_DL_EVENT event);

    void OnDownloaderSpeedCallback(int64_t id, double kps_per_sec, double data_loaded);

    void OnDownloaderResultCallback(int64_t id, const FpaDownloadResult& download_result);

private:
    jobject java_downloader_observer_;
    JavaVM* jvm_;
};

}
}

#endif //FPA_DEMO_SDK_AGORA_FPA_DOWNLOADER_OBSERVER_H

//
//
//  Created by tnj in 2021-08.
//  Copyright (c) 2021 Agora IO. All rights reserved.
//

#pragma once

#include "agora_downloader_base.h"
#include "agora_downloader_callback.h"
#include "agora_fpa_downloader_base.h"
#include "agora_event.h"
#include "include/low_level_api/agora_socket.h"
#include <thread>
#include <chrono>

namespace agora {
namespace fpa {

class AgoraDownloaderEventHandler : public IAgoraDownloaderEventHandler {
 public:
  void OnTokenWillExpire(const char* token) override;

  void OnNetworkTypeChanged(NETWORK_TYPE type) override;
};

class AgoraDownloaderImpl : public IFPADownloader {
 public:
  AgoraDownloaderImpl();

  virtual ~AgoraDownloaderImpl();

  int StartDownload(const FpaDownloadConfig& download_config);

  int StopDownload();

  void RegisterDownloadObserver(std::shared_ptr<IAgoraDownloaderObserver> observer);

  void UnregisterDownloadObserver();

 private:
  void DeleteAgoraSocket();

  void UpdateDownloaderEvent(FPA_DL_EVENT event);

  int DownloaderInit();

  int Connect(int chan_id);

  int DownloaderThread();

  int StartDownloadInternal();

  int AgoraSocketReadData(int& recv_length);

  double DownloadTimeDiff(const std::chrono::time_point<std::chrono::steady_clock, std::chrono::duration<double>>& start_time,
                        const std::chrono::time_point<std::chrono::steady_clock, std::chrono::duration<double>>& end_time);

  void OnDownloaderSpeedCallback(double kps_per_sec, double data_loaded);

  void OnDownloaderResultCallback(const FpaDownloadResult& download_result);

 private:
  bool is_starting_;
  agora_socket_conf socket_conf_;
  agora_socket_context* socket_context_;
  agora_socket* socket_;
  int socket_fd_;
  std::shared_ptr<IAgoraDownloaderObserver> observer_;
  std::unique_ptr<std::thread> thread_id_;
  bool need_download_;
  FpaDownloadConfig download_config_;
  agora::util::common::AgoraEvent agora_event_;
  std::chrono::time_point<std::chrono::steady_clock, std::chrono::duration<double>> start_time_;
  std::chrono::time_point<std::chrono::steady_clock, std::chrono::duration<double>> connect_end_time_;
};

}  // namespace fpa
}  // namespace agora

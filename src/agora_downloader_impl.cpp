//
//
//  Created by tnj in 2021-08.
//  Copyright (c) 2021 Agora IO. All rights reserved.
//
#include "agora_downloader_impl.h"
#include "agora_platforms_config.h"

#include <cstdlib>
#include <cstring>
#include "include/low_level_api/AgoraBase.h"

#define BUF_SIZE (65536)

namespace agora {
namespace fpa {

AgoraDownloaderImpl::AgoraDownloaderImpl()
    : socket_(nullptr),
      socket_context_(nullptr),
      socket_fd_(-1),
      is_starting_(false),
      need_download_(false) { }

AgoraDownloaderImpl::~AgoraDownloaderImpl() {
  StopDownload();
}

int AgoraDownloaderImpl::DownloaderThread(){
  agora_event_.AgoraEventWait();
  return StartDownloadInternal();
}

int AgoraDownloaderImpl::StartDownload(const FpaDownloadConfig& download_config) {
  download_config_ = download_config;
  need_download_ = true;
  thread_id_ = std::make_unique<std::thread>(&AgoraDownloaderImpl::DownloaderThread, this);
  agora_event_.AgoraEventNotify();
  return -agora::ERR_OK;
}

int AgoraDownloaderImpl::StopDownload() { 
  if(need_download_){
    agora_event_.AgoraEventNotify();
  }
  need_download_ = false;
  if(thread_id_){
    thread_id_->join();
    thread_id_ = nullptr;
  }
  DeleteAgoraSocket(); 
  is_starting_ = false;
}

void AgoraDownloaderImpl::RegisterDownloadObserver(std::shared_ptr<IAgoraDownloaderObserver> observer) {
  observer_ = observer;
}

void AgoraDownloaderImpl::UnregisterDownloadObserver() {
  observer_.reset();
}

void AgoraDownloaderImpl::DeleteAgoraSocket() {
  if (socket_fd_ >= 0 && socket_) {
    agora_socket_close(socket_);
  }
  socket_fd_ = -1;
  socket_ = nullptr;
  if (socket_context_) {
    agora_socket_context_free(socket_context_);
  }
  socket_context_ = nullptr;
  is_starting_ = false;
}

void AgoraDownloaderImpl::UpdateDownloaderEvent(FPA_DL_EVENT event) {
  if (observer_) {
    auto downloader_observer = observer_;
    auto thread_sig = static_cast<int64_t>(thread_id_->native_handle());
    downloader_observer->OnDownloaderEventCallback(thread_sig, event);
  }
}

void AgoraDownloaderImpl::OnDownloaderSpeedCallback(double kps_per_sec, double data_loaded){
  if (!observer_) {
    auto downloader_observer = observer_;
    auto thread_sig = static_cast<int64_t>(thread_id_->native_handle());
    downloader_observer->OnDownloaderSpeedCallback(thread_sig, kps_per_sec, data_loaded);
  }
}

void AgoraDownloaderImpl::OnDownloaderResultCallback(const FpaDownloadResult& download_result){
  if (!observer_) {
    auto downloader_observer = observer_;
    auto thread_sig = static_cast<int64_t>(thread_id_->native_handle());
    downloader_observer->OnDownloaderResultCallback(thread_sig, download_result);
  }
}

int AgoraDownloaderImpl::DownloaderInit() {
  UpdateDownloaderEvent(FPA_DL_EVENT::FPA_DL_EVENT_DISCONNECTED);
  socket_context_ = agora_socket_context_new(&socket_conf_);
  if (!socket_context_) {
    return -agora::ERR_FAILED;
  }
  socket_ = agora_socket_open(socket_context_);
  if (!socket_) {
    DeleteAgoraSocket();
    return -agora::ERR_FAILED;
  }
  return static_cast<int>(agora::ERR_OK);
}

int AgoraDownloaderImpl::Connect(int chan_id) {
  UpdateDownloaderEvent(FPA_DL_EVENT::FPA_DL_EVENT_CONNECTING);
  start_time_ = std::chrono::steady_clock::now();
  socket_fd_ = agora_socket_tcp_connect(socket_context_, chan_id, 1, "", 0, nullptr);
  if (socket_fd_ < 0) {
    UpdateDownloaderEvent(FPA_DL_EVENT::FPA_DL_EVENT_FAILED);
    return socket_fd_;
  }
  connect_end_time_ = std::chrono::steady_clock::now();
  UpdateDownloaderEvent(FPA_DL_EVENT::FPA_DL_EVENT_CONNECTED);
  return static_cast<int>(agora::ERR_OK);
}

int AgoraDownloaderImpl::AgoraSocketReadData(int& recv_length){
  int header_len = 0;
  int body_len = 0;
  int content_len = 0;
  char buf[BUF_SIZE] = {0};
  while(true){
    int res = agora_socket_read(socket_, buf + recv_length, BUF_SIZE - recv_length);
    if (res < 0) {
      #if defined(ANDROID)
      if (errno == -EAGAIN) {
        continue;
      }
      #endif
      if(res < 0){
        printf("[rtns-tcp-demo] wrapper fd read failed, errno: %d, recv_length: %d\n", res, recv_length);
        DeleteAgoraSocket();
        return res;
      }
    }
    if (res == agora::ERR_OK) {
      break;
    }
    recv_length += res;

    char *p = strstr(buf, "\r\n\r\n");
    if (p == NULL) {
      if (recv_length >= BUF_SIZE) {
        printf("error: too big http header\n");
        DeleteAgoraSocket();
        return -agora::ERR_FAILED;
      }
      continue;
    }

    header_len = (p - buf) + 4;
 
    p = strcasestr(buf, "content-length:");
    if (p == NULL) {
      printf("error: can not find content-length\n");
      DeleteAgoraSocket();
      return -agora::ERR_FAILED;
    }
 
    p += sizeof("content-length:") - 1;
 
    while (*p == ' ') {
      p++;
    }
 
    while (isdigit(*p)) {
      content_len *= 10;
      content_len += *p - '0';
      p++;
    }
 
    body_len = recv_length - header_len;
    break;
  }
  return body_len;
}

double AgoraDownloaderImpl::DownloadTimeDiff(const std::chrono::time_point<std::chrono::steady_clock, std::chrono::duration<double>>& start_time,
                                            const std::chrono::time_point<std::chrono::steady_clock, std::chrono::duration<double>>& end_time){
  auto diff_time = std::chrono::duration<double>(end_time - start_time);
  return diff_time.count();
}

int AgoraDownloaderImpl::StartDownloadInternal(){
  if (is_starting_) {
    return -agora::ERR_ALREADY_IN_USE;
  }
  if(!need_download_){
    return agora::ERR_OK;
  }
  int err = 0;
  is_starting_ = true;
  socket_conf_.app_id = download_config_.app_id;
  socket_conf_.token = download_config_.token;
  err = DownloaderInit();
  if (err != agora::ERR_OK) {
    return err;
  }

  err = Connect(download_config_.chan_id);
  if (err != agora::ERR_OK) {
    return err;
  }
  UpdateDownloaderEvent(FPA_DL_EVENT::FPA_DL_EVENT_START);  // start download
  double loaded_data = 0;
  int recv_len = 0;
  FpaDownloadResult downloader_result;
  while(need_download_){
    auto prev_time = std::chrono::steady_clock::now();
    int body_len = AgoraSocketReadData(recv_len);
    if(body_len < 0){
      break;
    }
    loaded_data += body_len;
    auto now = std::chrono::steady_clock::now();
    double diff_time = DownloadTimeDiff(prev_time, now);
    double kps_per_sec = body_len / diff_time * 8 / 1024;
    OnDownloaderSpeedCallback(kps_per_sec, loaded_data);
    if(recv_len < BUF_SIZE){
      diff_time = DownloadTimeDiff(start_time_, now);  //s
      downloader_result.total_time = diff_time;  //s
      downloader_result.avg_dl_speed = loaded_data / diff_time * 8 / 1024;  //kps
      downloader_result.conn_time_cast = DownloadTimeDiff(start_time_, connect_end_time_);
      OnDownloaderResultCallback(downloader_result);
      UpdateDownloaderEvent(FPA_DL_EVENT::FPA_DL_EVENT_END);  //download end
      break;
    }
    recv_len = 0;
  }
  need_download_ = false;
}

}  // namespace fpa
}  // namespace agora

//
//
//  Created by tnj in 2021-08.
//  Copyright (c) 2021 Agora IO. All rights reserved.
//

#pragma once

namespace agora {
namespace fpa {

enum LOG_LEVEL {
  LOG_LEVEL_INDICATION = 0,  // indication no log output
  LOG_LEVEL_INFO = 1,        // information level
  LOG_LEVEL_WARNING = 2,     // warning level
  LOG_LEVEL_ERROE = 3,       // error level
  LOG_LEVEL_FATAL = 4,       // fatal level
};

enum NETWORK_TYPE {
  NETWORK_TYPE_UNKNOWN = -1,     // unknown type
  NETWORK_TYPE_DISCONECTED = 0,  // network is disconnected
  NETWORK_TYPE_LAN = 1,          // LAN
  NETWORK_TYPE_WIFI = 2,         // WIFI
  NETWORK_TYPE_MOBILE_2G = 3,    // mobile 2G
  NETWORK_TYPE_MOBILE_3G = 4,    // mobile 3G
  NETWORK_TYPE_MOBILE_4G = 5,    // mobile 4G
};

struct AgoraDownloaderConfig {
  /**
   * The pointer to the app ID.
   */
  const char* app_id;
  /**
   * The pointer to the token.
   */
  const char* token;
  /**
   * The pointer to the log file path.
   */
  const char* log_file_path;
  /**
   * The log file size in kbytes.
   */
  int file_size_in_kb;
  /**
   * The log level
   * - 0 indication no log output
   * - 1 information level
   * - 2 warning level
   * - 4 error level
   * - 8 fatal level
   */
  int log_level;
};

enum FPA_DL_EVENT {
  FPA_DL_EVENT_IDLE = 0,
  FPA_DL_EVENT_START,
  FPA_DL_EVENT_END,
  FPA_DL_EVENT_FAILED,
  FPA_DL_EVENT_DISCONNECTED,
  FPA_DL_EVENT_CONNECTING,
  FPA_DL_EVENT_CONNECTED,
};

enum FPA_DL_MODE {
  FPA_DL_NORMAL_NETWORK = 0,
  FPA_DL_AGORA_FPA = 1,
};

struct FpaDownloadResult {
  double total_time;
  double avg_dl_speed;
  double conn_time_cast;
};

struct FpaDownloadConfig {
  FPA_DL_MODE mode;  // 0: normal download 1: via fpa.
  char* url;         // url for normal download
  char* token;       // token
  int chan_id;       // fpa : channel ID
  char* app_id;      // fpa: app id
  char* name;        // fpa : resource name
};

}  // namespace fpa
}  // namespace agora

//
//
//  Created by tnj in 2021-08.
//  Copyright (c) 2021 Agora IO. All rights reserved.
//

#pragma once

#include <cstdint>

#include "agora_downloader_base.h"

namespace agora {
namespace fpa {

class IAgoraDownloaderEventHandler {
 public:
  /**
   * Triggered when token privilege will expire
   * @param token The pointer to the token
   */
  virtual void OnTokenWillExpire(const char* token) = 0;

  /**
   * Triggered when network type changed
   * @param type The network type
   */
  virtual void OnNetworkTypeChanged(NETWORK_TYPE type) = 0;

  virtual ~IAgoraDownloaderEventHandler() = default;
};

class IAgoraDownloaderObserver {
 public:
  virtual void OnDownloaderEventCallback(int64_t id, FPA_DL_EVENT event) = 0;

  virtual void OnDownloaderSpeedCallback(int64_t id, double kps_per_sec, double data_loaded) = 0;

  virtual void OnDownloaderResultCallback(int64_t id, const FpaDownloadResult& download_result) = 0;
};

}  // namespace fpa
}  // namespace agora

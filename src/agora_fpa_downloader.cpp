//
//
//  Created by tnj in 2021-08.
//  Copyright (c) 2021 Agora IO. All rights reserved.
//
#include "agora_fpa_downloader.h"

#include "include/low_level_api/AgoraBase.h"
#include "agora_downloader_base.h"

namespace agora {
namespace fpa {

AgoraFpaDownloaderImpl::AgoraFpaDownloaderImpl() : start_download_(false) {}

AgoraFpaDownloaderImpl::~AgoraFpaDownloaderImpl() { StopDownload(); }

int AgoraFpaDownloaderImpl::StartDownload(const FpaDownloadConfig& download_config) {
  if(start_download_){
    return -agora::ERR_ALREADY_IN_USE;
  }
  download_config_ = download_config;
  if (download_config_.mode == FPA_DL_MODE::FPA_DL_AGORA_FPA) {
    start_download_ = true;
    downloader_ = std::make_unique<AgoraCurlDownloader>();
    downloader_->RegisterDownloadObserver(observer_.lock());
    return downloader_->StartDownload(download_config);
  } else if (download_config_.mode == FPA_DL_MODE::FPA_DL_NORMAL_NETWORK) {
    start_download_ = true;
    downloader_ = std::make_unique<AgoraDownloaderImpl>();
    downloader_->RegisterDownloadObserver(observer_.lock());
    return downloader_->StartDownload(download_config);
  }
  return -agora::ERR_NOT_SUPPORTED;
}

int AgoraFpaDownloaderImpl::StopDownload() {
  if (!start_download_) {
    return -agora::ERR_NOT_SUPPORTED;
  }
  if (download_config_.mode == FPA_DL_MODE::FPA_DL_AGORA_FPA ||
      download_config_.mode == FPA_DL_MODE::FPA_DL_NORMAL_NETWORK) {
    downloader_->RegisterDownloadObserver(observer_.lock());
    downloader_->StopDownload();
  } 
  start_download_ = false;
  return -agora::ERR_NOT_SUPPORTED;
}

void AgoraFpaDownloaderImpl::RegisterDownloadObserver(std::shared_ptr<IAgoraDownloaderObserver> observer) {
  observer_ = observer;
}

void AgoraFpaDownloaderImpl::UnregisterDownloadObserver() { 
  observer_.reset();
}

}  // namespace fpa
}  // namespace agora

//
//
//  Created by tnj in 2021-08.
//  Copyright (c) 2021 Agora IO. All rights reserved.
//
#include "agora_curl_downloader.h"
#include "include/low_level_api/AgoraBase.h"
#include <string>

namespace agora {
namespace fpa {

AgoraCurlDownloader::AgoraCurlDownloader()
                    : start_download_(false),
                    curl_handle_(nullptr),
                    need_download_(false),
                    thread_id_(nullptr) { }

AgoraCurlDownloader::~AgoraCurlDownloader(){
  StopDownload();
}

int AgoraCurlDownloader::DownloaderThread(){
  agora_event_.AgoraEventWait();
  return StartDownloadInternal();
}

int AgoraCurlDownloader::CurlCloseSocket(void *clientp, curl_socket_t item) {
  dl_event_ = agora::fpa::FPA_DL_EVENT_DISCONNECTED;
  DownloaderEventUpdate(FPA_DL_EVENT::FPA_DL_EVENT_DISCONNECTED);
  return 0;
}

size_t AgoraCurlDownloader::ReadHeaderFunction( void *ptr, size_t size, size_t nmemb, void *stream){
    if (dl_event_ == agora::fpa::FPA_DL_EVENT_CONNECTING) {
        dl_event_ = agora::fpa::FPA_DL_EVENT_CONNECTED;
        DownloaderEventUpdate(FPA_DL_EVENT::FPA_DL_EVENT_CONNECTED);
    }
    char head[2048] = {0};
    memcpy(head,ptr,size*nmemb+1);
    return size*nmemb;
}

int AgoraCurlDownloader::StartDownloadInternal(){
  if (dl_event_ != FPA_DL_EVENT::FPA_DL_EVENT_IDLE || !start_download_) {
    return agora::ERR_OK;
  }
  const char* url = download_config_.url;
  CURLcode ret;
  curl_handle_ = curl_easy_init();   
  if (curl_handle_ == NULL) {
    DownloaderEventUpdate(FPA_DL_EVENT::FPA_DL_EVENT_FAILED);
    return agora::ERR_FAILED;
  }

  DownloaderEventUpdate(FPA_DL_EVENT::FPA_DL_EVENT_START);
  ret = curl_easy_setopt(curl_handle_, CURLOPT_HEADERFUNCTION, &AgoraCurlDownloader::ReadHeaderFunction);
  ret = curl_easy_setopt(curl_handle_, CURLOPT_CLOSESOCKETFUNCTION, &AgoraCurlDownloader::CurlCloseSocket);
  ret = curl_easy_setopt(curl_handle_, CURLOPT_PROGRESSFUNCTION, &AgoraCurlDownloader::CurlDlProgressCallback);
  printf("curl fetch code %d\n", ret);

  ret = curl_easy_setopt(curl_handle_, CURLOPT_URL, url);
  ret = curl_easy_setopt(curl_handle_, CURLOPT_NOPROGRESS, 0L);
  ret = curl_easy_setopt(curl_handle_, CURLOPT_TIMEOUT, 30); 

  DownloaderEventUpdate(FPA_DL_EVENT_CONNECTING);
  ret = curl_easy_perform(curl_handle_);
  

  double time;
  curl_easy_getinfo(curl_handle_, CURLINFO_TOTAL_TIME, &time);
  printf("CURLINFO_TOTAL_TIME = %.2f\n", time);
  double conn_time;
  curl_easy_getinfo(curl_handle_, CURLINFO_CONNECT_TIME, &conn_time);
  printf("CURLINFO_CONNECT_TIME = %.2f\n", conn_time);
  int res_code = 0;
  curl_easy_getinfo(curl_handle_, CURLINFO_RESPONSE_CODE, &res_code);
  double avg_speed;
  curl_easy_getinfo(curl_handle_, CURLINFO_SPEED_DOWNLOAD, &avg_speed);
  printf("CURLINFO_SPEED_DOWNLOAD = %.2f\n", avg_speed);

  if (ret != CURLE_OK) {
    if (ret == CURLE_HTTP_RETURNED_ERROR) {  // >400
      printf("CURLE_HTTP_RETURNED_ERROR\n");
    }
    DownloaderEventUpdate(FPA_DL_EVENT::FPA_DL_EVENT_FAILED);
    return agora::ERR_FAILED;
  } else {
    FpaDownloadResult result;
    result.total_time = time;
    result.conn_time_cast = conn_time;
    result.avg_dl_speed = avg_speed;
    DownloaderResultCallback(result);
  }
  CurlDownloaderDestroy();
  return agora::ERR_OK;
}

int AgoraCurlDownloader::StartDownload(const FpaDownloadConfig& download_config) {
  download_config_ = download_config;
  start_download_ = true;
  thread_id_ = std::make_unique<std::thread>(&AgoraCurlDownloader::DownloaderThread, this);
  agora_event_.AgoraEventNotify();
  return agora::ERR_OK;
}

int AgoraCurlDownloader::StopDownload() {
  if(need_download_){
    agora_event_.AgoraEventNotify();
  }
  start_download_ = false;
  need_download_ = false;
  if(thread_id_){
    thread_id_->join();
    thread_id_ = nullptr;
  }
  CurlDownloaderDestroy();
  return agora::ERR_OK;
}

void AgoraCurlDownloader::RegisterDownloadObserver(std::shared_ptr<IAgoraDownloaderObserver> observer) {
  observer_ = observer;
}

void AgoraCurlDownloader::UnregisterDownloadObserver() { 
  observer_.reset();
}

void AgoraCurlDownloader::DownloaderEventUpdate(FPA_DL_EVENT event){
  if(observer_){
    auto downloader_observer = observer_;
    auto thread_sig = static_cast<int64_t>(thread_id_->native_handle());
    downloader_observer->OnDownloaderEventCallback(thread_sig, event);
  }
}

void AgoraCurlDownloader::DownloaderResultCallback(const FpaDownloadResult& download_result){
  if(observer_){
    auto downloader_observer = observer_;
    auto thread_sig = static_cast<int64_t>(thread_id_->native_handle());
    downloader_observer->OnDownloaderResultCallback(thread_sig, download_result);
  }
}

void AgoraCurlDownloader::DownloaderSpeedCallback(double kps_per_sec, double data_loaded){
  if(observer_){
    auto downloader_observer = observer_;
    auto thread_sig = static_cast<int64_t>(thread_id_->native_handle());
    downloader_observer->OnDownloaderSpeedCallback(thread_sig, kps_per_sec, data_loaded);
  }
}

int AgoraCurlDownloader::CurlDlProgressCallback(void* clientp, double dltotal, double dlnow, double ultotal,
                              double ulnow){
  if (dl_event_ == FPA_DL_EVENT::FPA_DL_EVENT_CONNECTED) {
    double speed = 0;
    curl_easy_getinfo(curl_handle_, CURLINFO_SPEED_DOWNLOAD, &speed);
    DownloaderSpeedCallback(speed, dlnow);
  }
  return agora::ERR_OK;
}

void AgoraCurlDownloader::CurlDownloaderDestroy(){
  curl_easy_cleanup(curl_handle_);
  DownloaderEventUpdate(FPA_DL_EVENT::FPA_DL_EVENT_IDLE);
}

}  // namespace fpa
}  // namespace agora

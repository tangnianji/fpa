//
//
//  Created by tnj in 2021-08.
//  Copyright (c) 2021 Agora IO. All rights reserved.
//

#include "agora_event.h"

namespace agora{
namespace util{
namespace common{

AgoraEvent::AgoraEvent() : is_notify_(false) { }

void AgoraEvent::AgoraEventNotify(){
  std::lock_guard<std::mutex> lock(agora_mutex_);
  agora_cond_.notify_one();
  is_notify_ = true;
}

void AgoraEvent::AgoraEventWait(){
  std::unique_lock<std::mutex> lock(agora_mutex_);
  while(!is_notify_){
     agora_cond_.wait(lock);
  }
  is_notify_ = false;
}

}
}
}

//
//
//  Created by tnj in 2021-08.
//  Copyright (c) 2021 Agora IO. All rights reserved.
//

#pragma once

#include <memory>
#include <vector>

#include "agora_curl_downloader.h"
#include "agora_downloader_base.h"
#include "agora_downloader_callback.h"
#include "agora_downloader_impl.h"
#include "agora_fpa_downloader_base.h"
#include "include/low_level_api/agora_socket.h"

namespace agora {
namespace fpa {

class AgoraFpaDownloaderImpl : public IFPADownloader {
 public:
  AgoraFpaDownloaderImpl();
  virtual ~AgoraFpaDownloaderImpl();

  int StartDownload(const FpaDownloadConfig& download_config);

  int StopDownload();

  void RegisterDownloadObserver(std::shared_ptr<IAgoraDownloaderObserver> observer);

  void UnregisterDownloadObserver();

 private:
  bool start_download_;
  FpaDownloadConfig download_config_;
  std::unique_ptr<IFPADownloader> downloader_;
  std::weak_ptr<IAgoraDownloaderObserver> observer_;
};

}  // namespace fpa
}  // namespace agora

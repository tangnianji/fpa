//
//
//  Created by tnj in 2021-08.
//  Copyright (c) 2021 Agora IO. All rights reserved.
//

#pragma once

#include <memory>
#include <thread>
#include <condition_variable>
#include <mutex>

namespace agora{
namespace util{
namespace common{

class AgoraEvent{
public:
  AgoraEvent();

  void AgoraEventNotify();

  void AgoraEventWait();

private:
  std::mutex agora_mutex_;
  std::condition_variable agora_cond_;
  bool is_notify_;
};

}
}
}

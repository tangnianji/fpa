//
//
//  Created by tnj in 2021-08.
//  Copyright (c) 2021 Agora IO. All rights reserved.
//

#pragma once

#include <memory>
#include "agora_downloader_base.h"
#include "agora_downloader_callback.h"

namespace agora {
namespace fpa {

class IFPADownloader {
 public:
  virtual int StartDownload(const FpaDownloadConfig& download_config) = 0;

  virtual int StopDownload() = 0;

  virtual void RegisterDownloadObserver(std::shared_ptr<IAgoraDownloaderObserver> observer) = 0;

  virtual void UnregisterDownloadObserver() = 0;

  virtual ~IFPADownloader() = default;
};

}  // namespace fpa
}  // namespace agora

//
//
//  Created by tnj in 2021-08.
//  Copyright (c) 2021 Agora IO. All rights reserved.
//

#pragma once

#include <memory>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <include/curl/curl.h>
#include "agora_fpa_downloader_base.h"
#include "agora_downloader_base.h"
#include "agora_event.h"

namespace agora {
namespace fpa {

class AgoraCurlDownloader : public IFPADownloader {
 public:
  AgoraCurlDownloader();

  virtual ~AgoraCurlDownloader();

  int StartDownload(const FpaDownloadConfig& download_config);

  int StopDownload();

  void RegisterDownloadObserver(std::shared_ptr<IAgoraDownloaderObserver> observer);

  void UnregisterDownloadObserver();

 private:
  void DownloaderEventUpdate(FPA_DL_EVENT event);

  int DownloaderThread();

  int CurlDlProgressCallback(void* clientp, double dltotal, double dlnow, double ultotal,
                              double ulnow);

  void DownloaderResultCallback(const FpaDownloadResult& download_result);

  void DownloaderSpeedCallback(double kps_per_sec, double data_loaded);

  void CurlDownloaderDestroy();

  int StartDownloadInternal();

  int CurlCloseSocket(void *clientp, curl_socket_t item);

  size_t ReadHeaderFunction( void *ptr, size_t size, size_t nmemb, void *stream);

 private:
  std::shared_ptr<IAgoraDownloaderObserver> observer_;
  bool start_download_;
  bool need_download_;
  CURL* curl_handle_;
  FPA_DL_EVENT dl_event_;
  std::unique_ptr<std::thread> thread_id_;
  agora::util::common::AgoraEvent agora_event_;
  FpaDownloadConfig download_config_;
};

}  // namespace fpa
}  // namespace agora
